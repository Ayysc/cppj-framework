#include "Window.h"

#include <conio.h>
#include <assert.h>

void Window::Paint()
{
	for( int y = 0; y < SCR_HEIGHT; ++y )
	{
		for( int x = 0; x < SCR_WIDTH; ++x )
		{
			_putch( map[SCR_WIDTH * y + x] );
		}
		_putch( '\n' );
	}
}

void Window::Put( int x,int y,char ch )
{
	assert( x >= 0 && x < SCR_WIDTH &&
		y >= 0 && y < SCR_HEIGHT );

	map[SCR_WIDTH * y + x] = ch;
}