#pragma once

#include "Window.h"
#include "Keyboard.h"

class Game
{
public:
	void Go();
	void Update();
	void Draw();
private:
	Window wnd;
	Keyboard kbd;
};