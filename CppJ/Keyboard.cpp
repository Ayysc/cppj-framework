#include "Keyboard.h"

#include <conio.h>

char Keyboard::CheckKey() const
{
	return _getch();
}
