#pragma once

#include "Keyboard.h"

class Window
{
public:
	void Paint();

	void Put( int x,int y,char ch );
public:
	static constexpr int SCR_WIDTH = 5;
	static constexpr int SCR_HEIGHT = 3;
	Keyboard kbd;
private:
	char map[SCR_WIDTH * SCR_WIDTH] =
	{
		'-','-','-','-','-',
		'-','-','@','-','-',
		'-','-','-','-','-'
	};
};